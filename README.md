`Jig` is a minimal C library for event-based scanning of JSON data.
It uses global event callouts, so it can serve only one kind of scan in a program.
It is intended for fast extracts from a large volume of JSON records.

An event-based scan allows the program to work only with the data of interest.
JSON values can be collected into custom data structures
or streamed directly to output.

JSON data must be passed in as a complete string, and is scanned in a single pass.
This is well suited to the common format of single-line JSON records.

Using this effectively requires understanding the structure of the JSON input
and constructing parser state.
Data callouts pass pointers into the input stream, so any data copying has to be managed by the program.
Items that can appear out-of-order must be copied and saved until they are all seen.
Names that appear in several substructures must track the enclosing context.

The example `from-ts.c` can scan 9-10 times as fast as the `jq` equivalent.


# FUNCTIONS

The entry point is the function

    const char	*jig(const char *j);

which scans a complete record in the string `j`.    

# CALLOUTS

The application is responsible for implementing the following callouts,
which are linked directly against the `Jig` library.
They can be empty functions.

    void	jig_key(const char key[], int len);
    void	jig_val(const char val[], int len);
    void	jig_push(char bracket), jig_pop(char bracket);

`Jig` calls `jig_key` when it recognizes a JSON name.
The `key` and `len` point to the quoted name.

It calls `jig_val` when it recognizes a named value or an array element,
with `val` and `len` pointing to the value.
The value may be a string with its enclosing quotes, a number beginning with a sign or digit,
or one of the keywords `null`, `true`, or `false`.

`jig_push` and `jig_pop` are called for nested structures,
where the bracket is `{` for a name/value dictionary and `[` for an array.


# ERRORS

`Jig` does not attempt to validate its input.
However, it may detect an error in its own scan.
In this case it returns a pointer to the remainder of the input.
Otherwise it returns NULL.


# PATH

Nontrivial JSON structures may use the same name in different contexts.
To help with context, `jig` offers the following utilities.

`jig_pathv[]` is a stack of substructure names,
ranging from the outermost at `jig_pathv[0]` to the innermost at `jig_pathv[jig_pathc - 1]`.
The caller maintains the stack using `jig_pathpush` and `jig_pathpop`.
`jig_pathkey` sets the name at the current stack level, which is initially NULL.

Path keys can be anything.
Using known constant strings as keys lets them be compared as pointers instead of strings.

    extern unsigned	jig_pathc;
    extern const char	**jig_pathv;

    void	jig_pathpush(void), jig_pathpop(void);
    int	jig_pathkey(const char *key);


# TESTS

    gcc -Wall -c jig.c
    gcc -Wall -DTESTS jig.c -o jigtest
    gcc -Wall -DTESTS path.c jig.o -o pathtest

builds two test programs that run self-tests and then read JSON lines from `stdin`.


*   `jigtest` logs the parsing callouts.
*   `pathtest` prints all paths to members.

