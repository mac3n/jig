/* capture JSON .timestamp and all .from below top level
 * and output as .from\t.timestamp lines
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "jig.h"
#include "path.h"

/* hooks */
void	jig_push(char bracket) { jig_pathpush(); }
void	jig_pop(char bracket) { jig_pathpop(); }

/* our keys, as quoted strings */
static const char	*fromkey = "\"from\"", *timekey = "\"timestamp\"";

/* set the keys that we care about
 * use a constant so we can just compare pointers
 */
void
jig_key(const char key[], int len) {
	if (!strncmp(fromkey, key, len)) {
		jig_pathkey(fromkey);
	}
	else if (!strncmp(timekey, key, len)) {
		jig_pathkey(timekey);
	}
	else {
		jig_pathkey(NULL);
	}
}

/* timestamp */
static char	time[64];
/* vector of from addresses */
static char	(*fromv)[64] = NULL;
static int	fromc = 0;

/* capture .timestamp and nested .from */
void
jig_val(const char val[], int len) {
	if ((jig_pathc == 1) && (jig_pathv[jig_pathc - 1] == timekey)) {
		if (len < sizeof time) {
			/* capture number text */
			memcpy(time, val, len);
			time[len] = '\0';
		}
	}
	else if ((1 < jig_pathc) && (jig_pathv[jig_pathc - 1] == fromkey)) {
		if ((len < sizeof *fromv) && (*val=='"')) {
			/* strip quotes & push */
			val += 1;
			len -= 2;
			fromc++;
			fromv = realloc(fromv, fromc * sizeof *fromv);
			memcpy(fromv[fromc - 1], val, len);
			fromv[fromc - 1][len] = '\0';
		}
	}
}

/* collect .from fields and .timestamp and dump them, one to a line */
int
main(int argc, char *argv[]) {
	int ln = 1;
	char *line = NULL;
	size_t linesize = 0;
	while (0 < getline(&line, &linesize, stdin)) {
		const char *err = jig(line);
		if (err) {
			fprintf(stderr, "error:%d '%s'\n", ln, err);
			fputs(line, stderr);
		}
		if (*time && fromc) {
			for (int i = 0; i < fromc; i++) {
				printf("%s\t%s\n", fromv[i], time);
			}
		}
		fromc = 0;
		time[0] = '\0';
		ln++;
	}
	return 0;
}