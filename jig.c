#include "jig.h"
#include <stdlib.h>
#ifdef TESTS
#include <assert.h>
#endif

/* bracket stack */
static unsigned	 brackc = 0;
static char	*brackv = NULL;

static char brackpush(char bracket) {
	brackc++;
	brackv = realloc(brackv, brackc);
	brackv[brackc - 1] = bracket;
	return bracket;
}

static char bracktop(void) {
	if (!brackc) {
		return 0;
	}
	return brackv[brackc - 1];
}

static char brackpop(void) {
	if (!brackc) {
		return 0;
	}
	brackc--;
	return brackv[brackc];
}

#ifdef TESTS
/* test bracket stack */

static void
testbrack(void) {
	brackpush('{');
	brackpush('[');
	assert(bracktop() == '[');
	brackpop();
	assert(bracktop() == '{');
	brackpush('[');
	assert(bracktop() == '[');
	brackpop();
	brackpop();
	assert(brackc == 0);
}
#endif

/* scan through unescaped close quote */
static const char
*str(const char *j) {
	char last = 0;
	j++;
	while (*j) {
		if (last == '\\') {
			last = 0;
			j++;
		}
		else if (*j == '"') {
			/* string close */
			return j;
		}
		else {
			last = *j++;
		}
	}
	return j;
}

/* signed integers and floats */
static const char
*num(const char *j) {
	while (*j) {
		switch (*j) {
			case '+':
			case '-':
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '.':
			case 'E':
			case 'e':
				j++;
				break;
			default:
				return j;
		}
	}
	return 0;
}

/* unquoted reserved word true, false, null */
static const char
*word(const char *j) {
	while (('a' <= *j) && (*j <= 'z')) {
		j++;
	}
	return j;
}

#ifdef TESTS
/* test subparsers */

static void
testvals(void) {
	char *j0 = "\"stringy\" ";
	const char *j = str(j0);
	assert((j != j0) && (*j == '"'));
	j0 = "-123.0e4 ";
	j = num(j0);
	assert(*j == ' ');
	j0 = "bogus ";
	j = word(j0);
	assert(*j == ' ');
	j0 = "\"string\\\"inner\\\"quotes \\\\\" ";
	j = str(j0);
	assert(*j == '\"');
}
#endif


/* parse the structure and make callbacks
 * bail out with a pointer if we get confused
 */
const char
*jig(const char *j) {
	/* start of token */
	const char *j0 = NULL;
	char colon = 0;

	while (*j) {
		switch (*j) {

			/* ignore whitespace */
			case ' ':
			case '\t':
			case '\n':
				j++;
				break;

			/* key / value */
			case ':':
				if (bracktop() != '{') {
					return j;
				}
				colon = *j++;
				break;
			case ',':
				colon = 0;
				j++;
				break;

			/* brackets */
			case '[':
			case '{':
				jig_push(brackpush(*j++));
				colon = 0;
				break;
			case ']':
				if (!brackc || (bracktop() != '[')) {
					return j;
				}
				jig_pop(brackpop());
				j++;
				break;
			case '}':
				if (!brackc || (bracktop() != '{')) {
					return j;
				}
				jig_pop(brackpop());
				j++;
				break;

			/* string */
			case '"':
				j0 = j;
				j = str(j0);
				if (*j != '"') {
					return j;
				}
				j++;
				if (!colon && (bracktop() == '{')) {
					jig_key(j0, j - j0);
				}
				else {
					jig_val(j0, j - j0);
				}
				break;

			/* numeric value */
			case '+':
			case '-':
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				j0 = j;
				j = num(j0);
				if (!colon && (bracktop() == '{')) {
					return j;
				}
				jig_val(j0, j - j0);
				break;

			/* constant value true/false/null but not Infinity or NaN */
			case 't':
			case 'f':
			case 'n':
				j0 = j;
				j = word(j0);
				if (!colon && (bracktop() == '{')) {
					return j;
				}
				jig_val(j0, j - j0);
				break;

			default:
					return j;
		}
	}
	if (brackc) {
		return j;
	}
	return NULL;
}

#ifdef TESTS

/* tokenize lines to stdout */
#include <stdio.h>
#include <string.h>

void
jig_key(const char key[], int len) {
	fputs("key: '", stdout);
	while (len--) {
		fputc(*key++, stdout);
	}
	fputs("'\n", stdout);
}
void
jig_val(const char val[], int len) {
	fputs("val: '", stdout);
	while (len--) {
		fputc(*val++, stdout);
	}
	fputs("'\n", stdout);
}
void
jig_push(char bracket) {
	fprintf(stdout, "push: %c\n", bracket);
};
void
jig_pop(char bracket) {
	fprintf(stdout, "pop: %c\n", bracket);
}

static void
test(FILE *f) {
	char *line = NULL;
	size_t linesize = 0;
	while (0 < getline(&line, &linesize, f)) {
		const char *err;
		fputs(line, stderr);
		err = jig(line);
		if (err) {
			fprintf(stderr, "error: '%s'\n", err);
		}
	}
}

int
main(int argc, char *argv[]) {
	if (1 < argc) {
		test(!strcmp(argv[1], "-") ? stdin : fopen(argv[1], "r"));
	}
	else {
		testbrack();
		testvals();
	}
}
#endif
