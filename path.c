#include <stdlib.h>
#include <string.h>
#include "path.h"

#ifdef TESTS
#include <assert.h>
#endif

/* keep a path of json keys */

unsigned	jig_pathc = 0;
const char	**jig_pathv = NULL;

/* update current key */
int
jig_pathkey(const char *key) {
	jig_pathv[jig_pathc - 1] = key;
	return jig_pathc;
}

/* in response to '{' */
void
jig_pathpush(void) {
	jig_pathc++;
	jig_pathv = realloc(jig_pathv, jig_pathc * sizeof *jig_pathv);
	jig_pathkey(NULL);
}

void
jig_pathpop(void) {
	jig_pathc--;
}

#ifdef TESTS
static void
testpaths(void) {
	char *path[] = {"first","second"};
	assert(jig_pathc == 0);

	jig_pathpush();
	assert(jig_pathc == 1);
	assert(jig_pathv[0] == NULL);
	jig_pathkey(path[0]);
	assert(jig_pathv[0] == path[0]);

	jig_pathpush();
	assert(jig_pathc == 2);
	jig_pathkey(path[1]);
	assert(jig_pathv[0] == path[0]);
	assert(jig_pathv[1] == path[1]);
	
	jig_pathpop();
	assert(jig_pathc == 1);
	assert(jig_pathv[0] == path[0]);
	jig_pathpop();
	assert(jig_pathc == 0);
}
#endif


#ifdef TESTS
#include <stdio.h>
#include "jig.h"

/* hooks */
void	jig_push(char bracket) { jig_pathpush(); }

/* copy key without quotes */
void
jig_key(const char key[], int len) {
	char *k = malloc(len - 1);
	assert(key[0] == '"' && key[len - 1] == '"');
	key += 1;
	len -= 2;
	memcpy(k, key, len);
	k[len] = '\0';
	jig_pathkey(k);
}

/* free up keys */
void
jig_pop(char bracket) {
	const char *top = jig_pathv[jig_pathc - 1];
	if (top) {
		free((char *)top);
	}
	jig_pathpop();
}

/* dump key path and value */
void
jig_val(const char *val, int len) {
	for (int i = 0; i < jig_pathc; i++) {
		if (0 < i) {
			fputc('.', stdout);
		}
		fputs(jig_pathv[i] != NULL ?jig_pathv[i] : "[]", stdout);
	}
	fputc(':', stdout);
	while (len--) {
		fputc(*val++, stdout);
	}
	fputc('\n', stdout);
}

/* show key path to all values */
static void
test(FILE *f) {
	char *line = NULL;
	size_t linesize = 0;
	while (0 < getline(&line, &linesize, f)) {
		const char *err;
		fputs(line, stderr);
		err = jig(line);
		if (err) {
			fprintf(stderr, "error: '%s'\n", err);
		}
	}
}

int
main(int argc, char *argv[]) {
	if (1 < argc) {
		test(!strcmp(argv[1], "-") ? stdin : fopen(argv[1], "r"));
	}
	else {
		testpaths();
	}
}
#endif
