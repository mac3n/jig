/* Parse valid JSON string into callbacks without copying or constructing. */

/* callbacks for keys and values in json string
 * includes quotes for keys and quoted values
 */
void	jig_key(const char key[], int len);
void	jig_val(const char val[], int len);

/* callback for structuring
 * bracket is the {} or [] character
 */
void	jig_push(char bracket), jig_pop(char bracket);

/* tokenize string through callbacks
 * return NULL or error position
 */
const char	*jig(const char *j);
