/* an example that merely echos input lines */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "jig.h"

/* not the first item */
static char comma = '\0';

  /* stack hooks */
void
jig_push(char b) {
	if (comma) {
		fputc(comma, stdout);
	}
	comma = '\0';
	fputc(b, stdout);
}
void
jig_pop(char b) {
	comma = ',';
	fputc(b + 2, stdout);
}

/* copy text to output */
static void
echo(const char *text, int len) {
	while (0 < len--) {
		fputc(*text, stdout);
		text++;
	}
}

/* got a key
 * insert comma here, not before val
 */
void
jig_key(const char key[], int len) {
	if (comma) {
		fputc(comma, stdout);
		comma = '\0';
	}
	echo(key, len);
	fputc(':', stdout);
}

/* got a val
 * insert comma iff needed
 */
void
jig_val(const char val[], int len) {
	if (comma) {
		fputc(comma, stdout);
	}
	else {
		comma = ',';
	}	 
	echo(val, len);
}

int
main(int argc, char *argv[]) {
	int ln = 1;
	char *line = NULL;
	size_t linesize = 0;
	while (0 < getline(&line, &linesize, stdin)) {
		const char *err = jig(line);
		if (err) {
			fprintf(stderr, "error:%d '%s'\n", ln, err);
			fputs(line, stderr);
		}
		comma = '\0';
		fputc('\n', stdout);
		ln++;
	}
	return 0;
}
