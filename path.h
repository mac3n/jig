/* keep a path of json keys */

extern unsigned	jig_pathc;
extern const char	**jig_pathv;

/* in response to '{' */
void	jig_pathpush(void), jig_pathpop(void);
/* update current key */
int	jig_pathkey(const char *key);
